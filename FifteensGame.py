from tkinter import *
import random
import time

class FifteensGame:
    def __init__(self):
        self.block_info = {'size_part': 60, 'color_block': "blue", 'color_number': "white"}
        self.index_block ,self.index_name = [], []
        self.trie = True
        self.start_time = time.time()
        size = self.block_info['size_part']
        self.coords_standart = [(0,0),(0,size),(0,size*2),(0,size*3),(size,0),(size,size),(size,size*2),
                                (size,size*3),(size*2,0),(size*2,size),(size*2,size*2),(size*2,size*3),(size*3,0),(size*3,size),(size*3,size*2)]
        self.tk = Tk()
        self.tk.title('Gra')
        self.tk.resizable(0, 0)
        self.tk.wm_attributes("-topmost", 1)
        self.canvas = Canvas(self.tk, width=(self.block_info['size_part']*4)+1, height=(self.block_info['size_part']*4)+1, bd=0, highlightthickness=0)
        self.canvas.pack()
        self.canvas.bind_all("<Button-1>",self.selected_block)
        
    def place_block(self):
        random.shuffle(self.coords_standart)
        for i in range(15):
            self.index_block.append(self.canvas.create_rectangle(self.coords_standart[i][1], self.coords_standart[i][0], \
                                    self.coords_standart[i][1]+self.block_info['size_part'], self.coords_standart[i][0]+self.block_info['size_part'],fill=self.block_info['color_block']))
            self.index_name.append(self.canvas.create_text(self.coords_standart[i][1] + self.block_info['size_part']/2,self.coords_standart[i][0] + self.block_info['size_part']/2, \
                                                           text=str(i+1), fill=self.block_info['color_number'], font=('Helvetica', int(self.block_info['size_part']/4) )))
            
    def move(self,selected_block):
        left = True
        right = True
        up = True
        down = True
        for i in range(len(self.index_block)):
            if self.canvas.coords(self.index_block[selected_block])[0] + self.block_info['size_part'] == self.canvas.coords(self.index_block[i])[0] and \
                self.canvas.coords(self.index_block[selected_block])[1] == self.canvas.coords(self.index_block[i])[1] or \
                self.canvas.coords(self.index_block[selected_block])[0] + self.block_info['size_part'] == self.block_info['size_part']*4:
                right = False
            if self.canvas.coords(self.index_block[selected_block])[0] - self.block_info['size_part'] == self.canvas.coords(self.index_block[i])[0] and  \
                self.canvas.coords(self.index_block[selected_block])[1] == self.canvas.coords(self.index_block[i])[1] or \
                self.canvas.coords(self.index_block[selected_block])[0] < self.block_info['size_part']:
                left = False
            if self.canvas.coords(self.index_block[selected_block])[1] + self.block_info['size_part'] == self.canvas.coords(self.index_block[i])[1] and \
                self.canvas.coords(self.index_block[selected_block])[0] == self.canvas.coords(self.index_block[i])[0] or \
                self.canvas.coords(self.index_block[selected_block])[1] + self.block_info['size_part'] == self.block_info['size_part']*4:
                down = False
            if self.canvas.coords(self.index_block[selected_block])[1] - self.block_info['size_part'] == self.canvas.coords(self.index_block[i])[1] and \
                self.canvas.coords(self.index_block[selected_block])[0] == self.canvas.coords(self.index_block[i])[0] or \
                self.canvas.coords(self.index_block[selected_block])[1] < self.block_info['size_part']:
                up = False
                
        if up:
            self.canvas.move(self.index_block[selected_block],0,-self.block_info['size_part'])
            self.canvas.move(self.index_name[selected_block],0,-self.block_info['size_part'])
        if down:
            self.canvas.move(self.index_block[selected_block],0,self.block_info['size_part'])
            self.canvas.move(self.index_name[selected_block],0,self.block_info['size_part'])
        if right:
            self.canvas.move(self.index_block[selected_block],self.block_info['size_part'],0)
            self.canvas.move(self.index_name[selected_block],self.block_info['size_part'],0)
        if left:
            self.canvas.move(self.index_block[selected_block],-self.block_info['size_part'],0)
            self.canvas.move(self.index_name[selected_block],-self.block_info['size_part'],0)
    
    def selected_block(self,event):
        x, y = event.x, event.y
        if not self.end_game():
            for i in range(len(self.index_block)):
                if self.canvas.coords(self.index_block[i])[0] < x and self.canvas.coords(self.index_block[i])[2] > x and\
                   self.canvas.coords(self.index_block[i])[1] < y and self.canvas.coords(self.index_block[i])[3] > y:
                    self.move(i)
                    break
            
    def end_game(self):
        for i in range(len(self.index_block)):
            expected_x = (i % 4) * self.block_info['size_part']
            expected_y = (i // 4) * self.block_info['size_part']
            if self.canvas.coords(self.index_block[i])[0] != expected_x or self.canvas.coords(self.index_block[i])[1] != expected_y:
                return False
        return True

    def tick(self):
        if self.end_game() and self.trie:
            self.canvas.create_text(self.block_info['size_part']*2,1.8 * self.block_info['size_part'],\
                                    text = 'Game Over',fill='red',font=('Helvetica',int(self.block_info['size_part'] / 2.5)))
            self.canvas.create_text(self.block_info['size_part']*2,2.2 * self.block_info['size_part'],\
                                    text = 'Your time %s seconds' % int(time.time()-self.start_time),fill='red',font=('Helvetica',int(self.block_info['size_part'] / 4.5)))
            self.trie = False
            
    def run(self):
        self.place_block()
        while True:
            self.tick()
            self.tk.update()
        
game = FifteensGame()
game.run()
