# This is a simple puzzle in which you should arrange squares with numbers in order.

The puzzle is a set of 15 identical square dominoes with numbers printed on them, lying in a square box. 
The length of the side of the box is four times the length of the side of the domino, so one square field in the box remains empty. 
The goal of the game is to arrange the dominoes in ascending order of numbers by moving them inside the box, preferably making as few moves as possible.

Example of an unfinished puzzle:

![unfinished](screen1.png)

Example of the finished puzzle: 

![finished](screen2.png)

## License 
```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                           November 2024
 
Copyright (C) 2024 Yaroslav Nimets <yarik.nimetz@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.
 
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

You just DO WHAT THE FUCK YOU WANT TO.
```
